﻿using UnityEngine;
using System.Collections;

public class TestAnimator : MonoBehaviour {

	public Animator animator;
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.A)) {
			animator.SetTrigger("Move");
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			animator.SetTrigger("Resize");
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			animator.SetTrigger("Away");
		}

		if (Input.GetKeyDown(KeyCode.F)) {
			animator.SetTrigger("Travel");
		}

	}
}
