﻿using UnityEngine;
using System.Collections;

public class Tester : MonoBehaviour {

	public GameObject itemToAdd;
	public GameObject anotherItemToAdd;

	private GameObject controller;

	void Start () {

		//Salva o controller, para usar a vontade.
		this.controller = GameObject.FindGameObjectWithTag ("GameController");

	}

	// Update is called once per frame
	void Update () {
	
		//Aperte A para adicionar um item ao inventorio.
		if (Input.GetKeyDown (KeyCode.A)) {

			var menuController = controller.GetComponent<MenuController> ();
			var inventoryController = menuController.InventoryController;

			var success = inventoryController.AddItem (itemToAdd);
			Debug.Log (success);
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			var inventoryController = controller.GetComponent<MenuController> ().InventoryController;
			
			var success = inventoryController.AddItem (anotherItemToAdd);
			Debug.Log (success);
		}


	}
}
