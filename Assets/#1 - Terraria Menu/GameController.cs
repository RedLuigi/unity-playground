﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	private MenuController menuController;

	// Use this for initialization
	void Start () {
	
		menuController = GetComponent<MenuController> ();

	}
	
	// Update is called once per frame
	void Update () {

		//Se pressionar ESC, voltar.
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (menuController.Active) {
				menuController.ToogleMenu();
			}
		}

	}
}
