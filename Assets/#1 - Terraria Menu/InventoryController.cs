﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class InventoryController : MonoBehaviour {

	[Tooltip("O numero de itens que cabem neste menu, na horizontal.")]
	public int inventoryWidth = 8;
	[Tooltip("O numero de itens que cabem neste menu, na vertical.")]
	public int inventoryHeight = 5;
	[Tooltip("O espaco entre cada item, em pixels.")]
	public int padding = 7;

	public GameObject itemSlot;


	public Image ItemInHand { get; set; }

	public InventorySlot MouseOverSlot { get; set; }


	private GameObject[] inventory;



	void Start(){

		inventory = new GameObject[inventoryWidth * inventoryHeight];

		//Cria todos os slots do menu, posicionando-os no seu local apropriado.
		for (var y = 0; y < inventoryHeight; y++){
			for (var x = 0; x < inventoryWidth; x++) {

				var index = x+(y*inventoryWidth);

				inventory[index] = GameObject.Instantiate(itemSlot, transform.position, transform.rotation) as GameObject;
				inventory[index].transform.SetParent(transform, false);


				var rectTransform = inventory[index].GetComponent<RectTransform>();

				rectTransform.anchoredPosition = new Vector2(
					(x * rectTransform.sizeDelta.x) + (x * padding) + padding*2,
					(-y * rectTransform.sizeDelta.y) - (y * padding) - padding*2
				);

			}
		}


	}

	/// <summary>
	/// Adiciona o item no primeiro espaço vazio do inventorio.
	/// </summary>
	/// <returns><b>true</b> caso o item seja inserido com sucesso.</returns>
	/// <param name="item">O item que devera ser inserido no menu.</param>
	public bool AddItem (GameObject item) {

		foreach (var slot in inventory) {
			var itemSlot = slot.GetComponent<InventorySlot>();

			if (itemSlot.item == null) {
				itemSlot.AddItem(item);
				return true;
			}
		}

		return false;
	}

	void OnGUI() {

		if (ItemInHand != null) {

			var zeroPosition = new Vector2();
			
			RectTransformUtility.ScreenPointToLocalPointInRectangle(
				transform.root as RectTransform,
				new Vector2(),
				Camera.main,
				out zeroPosition
			);

			var itemPosition = new Vector2(
				-((Input.mousePosition.x/(float)Screen.width)*2-1) * zeroPosition.x,
				-((Input.mousePosition.y/(float)Screen.height)*2-1) * zeroPosition.y
			);

			ItemInHand.rectTransform.anchoredPosition = itemPosition;

		}

	}

}
