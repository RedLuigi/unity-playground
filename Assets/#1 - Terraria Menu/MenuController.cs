﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	[Tooltip("A tecla que abre ou fecha este menu.")]
	public KeyCode menuKey = KeyCode.B;
	public GameObject menu;

	private Canvas canvas;

	public InventoryController InventoryController { get; private set; }
	public HotkeyController HotkeyController { get; private set; }

	public bool Active {
		get {
			return canvas.enabled;
		}
		set {
			canvas.enabled = value;
		}
	}

	void Start () {

		InventoryController = menu.GetComponentInChildren<InventoryController> ();
		HotkeyController = menu.GetComponentInChildren<HotkeyController> ();

		canvas = menu.GetComponent<Canvas> ();

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (menuKey)) {
			ToogleMenu();
		}

		if (Active) {

		}

	}

	public void ToogleMenu() {

		Active = !Active;
	}
}
