﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventorySlot : MonoBehaviour {

	public GameObject item;
	[Tooltip("O objeto que exibira a foto do item no jogo.")]
	public GameObject itemDisplay;

	private Image imageDisplay;
	private Image backgroundDisplay;
	private InventoryController controller;

	void Start () {

		var display = GameObject.Instantiate (itemDisplay);
		display.transform.SetParent (transform);
		display.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ();

		backgroundDisplay = GetComponent<Image> ();
		imageDisplay = display.GetComponent<Image> ();
		imageDisplay.enabled = false;

		controller = GetComponentInParent<InventoryController> ();

	}


	/// <summary>
	/// Adiciona um item ao espaço, removendo o item anterior de la.
	/// </summary>
	/// <returns>O item que ocupava este espaço, ou null, caso nenhum item estivesse la.</returns>
	/// <param name="item">O novo item que ocupara este espaço.</param>
	public GameObject AddItem(GameObject item) {
		var oldItem = RemoveItem();

		imageDisplay.enabled = true;

		this.item = item;
		imageDisplay.sprite = item.GetComponent<SpriteRenderer> ().sprite;

		return oldItem;
	}

	/// <summary>
	/// Remove o item deste espaço.
	/// </summary>
	/// <returns>O item que foi removido.</returns>
	public GameObject RemoveItem() {

		//Se nao houver item no slot, nao precisa fazer nada.
		if (this.item == null)
			return null;

		var item = this.item;
		this.item = null;

		imageDisplay.sprite = null;
		imageDisplay.enabled = false;

		return item;
	}

	public void BeginDrag() {
		if (item == null)
			return;

		Debug.Log ("Começou a mover!");

		imageDisplay.rectTransform.SetParent (transform.root);
		controller.ItemInHand = imageDisplay;
	}

	public void EndDrag() {
		if (item == null)
			return;

		Debug.Log ("Acabou de mover.");

		controller.ItemInHand = null;
		imageDisplay.rectTransform.SetParent (transform);
		imageDisplay.rectTransform.anchoredPosition = new Vector2 ();

		//Se o item estiver sobre um slot, remover o item deste slot e adicionar no outro.
		if (controller.MouseOverSlot != null) {

			var otherItem = controller.MouseOverSlot.AddItem(RemoveItem());

			if (otherItem != null) AddItem(otherItem);

		}

	}


	public void MouseEnter() {
		controller.MouseOverSlot = this;

		Color c = backgroundDisplay.color;
		c.a = 0.5f;
		backgroundDisplay.color = c;

	}

	public void MouseExit() {
		if (controller.MouseOverSlot == this)
			controller.MouseOverSlot = null;
		
		Color c = backgroundDisplay.color;
		c.a = 0.3f;
		backgroundDisplay.color = c;
	}
}
