﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HotkeyController : MonoBehaviour {
	
	public Image[] hotkeys = new Image[3];

	public KeyCode nextHotkey = KeyCode.E;
	public KeyCode previousHotkey = KeyCode.Q;


	public GameObject HotkeyX {
		get {
			return hotkeys[currentHotkey].transform.Find("HotkeySlot-X").GetComponent<InventorySlot>().item;
		}
	}
	public GameObject HotkeyY {
		get {
			return hotkeys[currentHotkey].transform.Find("HotkeySlot-Y").GetComponent<InventorySlot>().item;;
		}
	}
	public GameObject HotkeyA {
		get {
			return hotkeys[currentHotkey].transform.Find("HotkeySlot-A").GetComponent<InventorySlot>().item;;
		}
	}
	public GameObject HotkeyB {
		get {
			return hotkeys[currentHotkey].transform.Find("HotkeySlot-B").GetComponent<InventorySlot>().item;;
		}
	}

	private int currentHotkey = 0;

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (nextHotkey)) {
			currentHotkey++;
			if (currentHotkey >= hotkeys.Length) currentHotkey = 0;
			UpdateGUI();
		}
		if (Input.GetKeyDown (previousHotkey)) {
			currentHotkey--;
			if (currentHotkey < 0) currentHotkey = hotkeys.Length-1;
			UpdateGUI();
		}

	}

	private void UpdateGUI() {

		//Exibe a hotkey atual, esconde o destaque das demais.
		for (var i = 0; i < hotkeys.Length; i++) {
			hotkeys[i].enabled = (currentHotkey == i);
		}

	}
}
